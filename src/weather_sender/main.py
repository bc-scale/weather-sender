# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import logging
from http import HTTPStatus

from fastapi import BackgroundTasks, Depends, FastAPI, HTTPException, Response, Security
from fastapi.responses import RedirectResponse

from weather_sender import __version__
from weather_sender.dependencies import (
    get_api_key,
    get_health_check,
    get_logger,
    get_openweather_settings,
    get_sendgrid_settings,
    get_telegram_settings,
)
from weather_sender.messaging import send_error_email, send_telegram_message
from weather_sender.models import (
    ForecastRequest,
    ForecastRequests,
    ForecastResult,
    ForecastResults,
    ForecastSourceData,
    ForecastTransformedData,
)
from weather_sender.openweather import (
    get_air_pollution_data,
    get_location_data,
    get_weather_data,
    transform_weather_data,
)
from weather_sender.settings import (
    OpenWeatherSettings,
    SendGridSettings,
    TelegramSettings,
)

API_TITLE = "Weather Sender"
API_DESCRIPTION = """
Web service which exposes an endpoint to send a Telegram message
with a daily weather forecast for a given location.

[OpenAPI docs for v1 endpoints](/api/v1/docs)
"""

app = FastAPI(
    title=API_TITLE,
    description=API_DESCRIPTION,
    version=__version__,
    redoc_url=None,
)


@app.get("/", include_in_schema=False)
@app.get("/redoc", include_in_schema=False)
@app.get("/swagger", include_in_schema=False)
async def redirect_to_docs():
    return RedirectResponse("/docs")


@app.get(
    "/health",
    tags=["health_check"],
    operation_id="get_health",
)
async def get_health(health_check=Depends(get_health_check)):
    """Verifies if the web service is healthy."""
    content, status_code, headers = health_check.run()
    return Response(content=content, status_code=status_code, headers=headers)


@app.get(
    "/version",
    tags=["version"],
    operation_id="get_version",
)
async def get_version():
    """Responds the semantic version of the web service."""
    return __version__


v1 = FastAPI(
    title=API_TITLE,
    description=API_DESCRIPTION,
    version=__version__,
    redoc_url=None,
)


def __send_one_weather_forecast(
    request: ForecastRequest,
    logger: logging.Logger,
    openweather_settings: OpenWeatherSettings,
    sendgrid_settings: SendGridSettings,
    telegram_settings: TelegramSettings,
):
    logger.info("Coordinates: (%f, %f)", request.latitude, request.longitude)
    logger.info("Language: %s", request.language)
    logger.info("Telegram chat ID: %d", request.chat_id)

    weather_data = None
    try:
        # Retrieve location, weather and air pollution data
        # from OpenWeather REST services.
        source_location_data = get_location_data(
            openweather_settings,
            request.latitude,
            request.longitude,
        )[0]
        source_weather_data = get_weather_data(
            openweather_settings,
            request.latitude,
            request.longitude,
            request.language,
            request.units,
        )
        source_air_pollution_data = get_air_pollution_data(
            openweather_settings,
            request.latitude,
            request.longitude,
        )

        # Transform weather data according to our needs.
        weather_data = transform_weather_data(
            request,
            source_weather_data,
            source_location_data,
            source_air_pollution_data,
            logger,
        )

        # Prepare and send the message to the specified Telegram chat ID.
        send_telegram_message(telegram_settings, request.chat_id, weather_data)

        # Return all used data to the caller.
        source_data = ForecastSourceData(
            weather=source_weather_data,
            location=source_location_data,
            air_pollution=source_air_pollution_data,
        )
        transformed_data = ForecastTransformedData(weather=weather_data)
        return ForecastResult(
            source_data=source_data, transformed_data=transformed_data
        )
    except Exception as ex:
        logger.error(
            "An error occurred while sending forecast for coordinates: (%f, %f)",
            request.latitude,
            request.longitude,
            exc_info=True,
        )
        send_error_email(sendgrid_settings, logger, request, ex)
        raise HTTPException(HTTPStatus.FAILED_DEPENDENCY) from ex


@v1.post(
    "/forecasts/send-one",
    tags=["weather_sender"],
    operation_id="send_one",
    response_model=ForecastResult,
    dependencies=[Security(get_api_key)],
)
async def send_one_weather_forecast(
    request: ForecastRequest,
    logger: logging.Logger = Depends(get_logger),
    openweather_settings: OpenWeatherSettings = Depends(get_openweather_settings),
    sendgrid_settings: SendGridSettings = Depends(get_sendgrid_settings),
    telegram_settings: TelegramSettings = Depends(get_telegram_settings),
):
    """Sends a weather forecast for given location to specified Telegram chat ID."""

    return __send_one_weather_forecast(
        request, logger, openweather_settings, sendgrid_settings, telegram_settings
    )


# pylint: disable=too-many-arguments
@v1.post(
    "/forecasts/send-many",
    tags=["weather_sender"],
    operation_id="send_many",
    response_model=ForecastResults,
    dependencies=[Security(get_api_key)],
)
async def send_many_weather_forecasts(
    requests: ForecastRequests,
    background_tasks: BackgroundTasks,
    logger: logging.Logger = Depends(get_logger),
    openweather_settings: OpenWeatherSettings = Depends(get_openweather_settings),
    sendgrid_settings: SendGridSettings = Depends(get_sendgrid_settings),
    telegram_settings: TelegramSettings = Depends(get_telegram_settings),
):
    """Sends multiple weather forecasts."""

    logger.info("Requests: %d", len(requests.requests))
    logger.info("Background: %s", requests.background)

    results = []
    for request in requests.requests:
        if requests.background:
            background_tasks.add_task(
                __send_one_weather_forecast,
                request,
                logger,
                openweather_settings,
                sendgrid_settings,
                telegram_settings,
            )
        else:
            results.append(
                __send_one_weather_forecast(
                    request,
                    logger,
                    openweather_settings,
                    sendgrid_settings,
                    telegram_settings,
                )
            )

    return ForecastResults(results=results)


app.mount("/api/v1", v1)
