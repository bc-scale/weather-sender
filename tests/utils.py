# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import json
import logging
import typing
from os import path

from fastapi import FastAPI
from hypothesis.strategies import builds, floats, integers, sampled_from

from weather_sender import dependencies, openweather
from weather_sender.models import ForecastRequest
from weather_sender.settings import OpenWeatherSettings, TelegramSettings
from weather_sender.translations import EN, IT

LANGS = [EN, IT]

MIN_LAT = -90
MAX_LAT = +90
MIN_LON = -180
MAX_LON = +180

STUB_SECURITY_API_KEY = "API_KEY"
STUB_OPENWEATHER_API_KEY = "API_KEY"
STUB_TELEGRAM_BOT_TOKEN = "BOT_TOKEN"

SAMPLES_DIR = "./tests/samples/"
WEATHER_SAMPLES = [
    "onecall_with_alerts.json",
    "onecall_without_alerts.json",
    "onecall_without_wind_gust.json",
]
LOCATION_SAMPLES = [
    "reverse_with_local_names.json",
    "reverse_without_local_names.json",
    "reverse_without_state.json",
]
AIR_POLLUTION_SAMPLES = [
    "air_pollution_empty.json",
    "air_pollution_full.json",
]


class DependencyOverrider:
    def __init__(
        self, app: FastAPI, overrides: typing.Mapping[typing.Callable, typing.Callable]
    ):
        self.overrides = overrides
        self._app = app
        self._old_overrides = {}

    def __enter__(self):
        for dep, new_dep in self.overrides.items():
            if dep in self._app.dependency_overrides:
                # Save existing overrides.
                self._old_overrides[dep] = self._app.dependency_overrides[dep]
            self._app.dependency_overrides[dep] = new_dep
        return self

    def __exit__(self, *args: typing.Any):
        for dep in self.overrides.keys():
            if dep in self._old_overrides:
                # Restore previous overrides.
                self._app.dependency_overrides[dep] = self._old_overrides.pop(dep)
            else:
                # Just delete the entry.
                del self._app.dependency_overrides[dep]


def clear_caches():
    logging.Logger.manager.loggerDict.clear()

    dependencies.get_health_check_settings.cache_clear()
    # dependencies.get_openweather_settings.cache_clear()
    dependencies.get_security_settings.cache_clear()
    dependencies.get_sendgrid_settings.cache_clear()
    dependencies.get_telegram_settings.cache_clear()


def get_stub_logger():
    return logging.getLogger(__name__)


def get_stub_openweather_settings():
    return OpenWeatherSettings(api_key=STUB_OPENWEATHER_API_KEY)


def get_stub_telegram_settings():
    return TelegramSettings(bot_token=STUB_TELEGRAM_BOT_TOKEN)


def load_sample(sample):
    with open(
        path.join(SAMPLES_DIR, sample), mode="r", encoding="utf-8"
    ) as sample_file:
        sample_json = sample_file.read()
        parsed_sample = json.loads(sample_json)
        return sample_json, parsed_sample


def transform_weather_data(
    weather_sample: str,
    location_sample: str,
    district_name: typing.Optional[str],
    air_pollution_sample: str,
    lang: str,
):
    (_, weather_data) = load_sample(weather_sample)
    (_, location_data) = load_sample(location_sample)
    (_, air_pollution_data) = load_sample(air_pollution_sample)
    return openweather.transform_weather_data(
        ForecastRequest(
            latitude=0,
            longitude=0,
            language=lang,
            chat_id=0,
            location_name=district_name,
        ),
        weather_data,
        location_data[0],
        air_pollution_data,
        get_stub_logger(),
    )


def build_forecast_request():
    return builds(
        ForecastRequest,
        latitude=floats(min_value=MIN_LAT, max_value=MAX_LAT),
        longitude=floats(min_value=MIN_LON, max_value=MAX_LON),
        language=sampled_from([EN, IT]),
        chat_id=integers(),
    )
