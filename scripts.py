import os
import sys
from subprocess import CalledProcessError, check_call, check_output

PROJECT_MODULE = "weather_sender"
DEFAULT_PORT = "8080"


def __call(command: str) -> None:
    try:
        check_call(command, shell=True)
    except CalledProcessError as error:
        sys.exit(error.returncode)


def __output(command: str) -> str:
    try:
        return check_output(command, shell=True, text=True)
    except CalledProcessError as error:
        sys.exit(error.returncode)


def git_version():
    version_number = __output("git describe --tags").strip().split("-", maxsplit=1)[0]
    commit_sha = __output("git rev-parse --short HEAD").strip()
    version = f"{version_number}+{commit_sha}"
    __call(f"poetry version {version}")


def start():
    port = os.getenv("PORT", DEFAULT_PORT)
    uvicorn_flags = (
        f"--port {port} --no-server-header --no-access-log --log-level debug --reload"
    )
    __call(f"uvicorn {PROJECT_MODULE}.main:app {uvicorn_flags}")


def test():
    coverage_flags = f"--cov-report xml:coverage.xml --cov {PROJECT_MODULE} --cov tests"
    __call(f"pytest --junitxml test-report.xml {coverage_flags} tests/")
